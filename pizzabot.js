/*
Context List:
- 1: Person sent a message but no action was done
- 2: Person ordered but did not choose pizza
- 3: Person ordered a pizza in the menu
- 4: Person provided its name for the delivery
- 5: Person provided its name and address for delivery, end
*/

const mysql = require('mysql');
const con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "pizza_michelangelo"
});

con.connect(function(err) {
  if (err) throw err;
});

function getActionAndMenu(callback) {
  getActionList(function(action) {
    getMenu(function(menu) {
      callback(action, menu)
    })
  })
}

function getMenu(callback) {
  let sql = "SELECT name, price FROM pizza_menu"
  con.query(sql, function (err, result) {
      if (err) throw err;
      callback(result)
    });
}

function getActionList(callback) {
  let sql = "SELECT action FROM list_action"
  con.query(sql, function (err, result) {
      if (err) throw err;
      callback(result)
    });
}

function getResponse(action_id, callback) {
  let sql = "SELECT sentence FROM response WHERE action_id =" + action_id
  con.query(sql, function (err, result) {
      if (err) throw err;
      callback(result[Math.floor(Math.random() * result.length)].sentence);
    });
}

function getContext(sender_id, callback) {
  let sql = "SELECT context, pizza, name, total FROM context WHERE sender_id = " + sender_id
  con.query(sql, function (err, result) {
      if (err) throw err;
      callback(result)
    });
}

function createContext(sender_id, context) {
  let sql = "INSERT INTO context (sender_id, context) VALUES (" + sender_id + ", " + context + ")"
  con.query(sql, function (err, result) {
      if (err) throw err;
      return;
    });
}

function setContext(sender_id, context, pizza, total, name) {
  let sql
  if (typeof pizza == 'undefined') {
    sql = "UPDATE context SET context = " + context + " WHERE sender_id =" + sender_id;
  }
  else if (typeof pizza != 'undefined' && typeof name == 'undefined') {
    sql = "UPDATE context SET context = " + context + ", pizza = '" + pizza + "', total = " + total + " WHERE sender_id =" + sender_id;
  }
  else if (name != 'undefined') {
    sql = "UPDATE context SET context = " + context + ", pizza = '" + pizza + "', name = '" + name + "', total = " + total + " WHERE sender_id =" + sender_id;
  }
  con.query(sql, function (err, result) {
      if (err) throw err;
      return;
    });
}

function createOrder(pizza, name, address, total) {
  let sql = "INSERT INTO order_list (pizza, name, address, total) VALUES ('" + pizza + "', '" + name + "', '" + address + "', " + total + ")"
  con.query(sql, function (err, result) {
      if (err) throw err;
      return;
    });
}

function prepareMenu(menu, callback) {
  getResponse(2, function(response) {
    menu.forEach(function(item, index) {
      response += "\nPizza " + item.name + " - " + item.price + "$"
      if (index == menu.length - 1) {
        callback(response)
      }
    })
  })
}

function notOrdered(text, sender_id, context, callback) {
  let words = text.split(" ");
  let pizza = null;
  let total;
  let action = null
  getActionAndMenu(function(action_list, menu) {
    words.forEach(function(entry, index) {
      entry = entry.toLowerCase()
      if (action == null) {
        action_list.forEach(function(actionObject) {
          if (entry.substr(0, actionObject.action.length) == actionObject.action && action == null) {
            action = actionObject.action
          }
        })
      }
      if (pizza == null) {
        menu.forEach(function(menuObject) {
          if (entry.substr(0, menuObject.name.length) == menuObject.name.toLowerCase() && pizza == null) {
            pizza = menuObject.name
            price = menuObject.price
          }
        })
      }
      if (action == "menu") {
        prepareMenu(menu, function(response) {
          callback(response)
        })
      }
      if (context == 2) {
        menu.forEach(function(menuObject) {
          if (entry.substr(0, menuObject.name.length) == menuObject.name && pizza == null) {
            pizza = menuObject.name
            price = menuObject.price
            setContext(sender_id, 3, pizza, price)
            getResponse(4, function(response) {
              callback(response)
            })
          }
        })
      }
      if (index == words.length - 1) {
        if (action == "order" && pizza != null) {
          setContext(sender_id, 3, pizza, price)
          getResponse(4, function(response) {
            callback(response)
          })
        }
        else if (action == "order" && pizza == null) {
          setContext(sender_id, 2)
          getResponse(1, function(response1) {
            prepareMenu(menu, function(response2) {
              callback(response1, response2)
            })
          })
        }
        else if (context == 2 && pizza != null) {
          setContext(sender_id, 3, pizza, price)
          getResponse(4, function(response) {
            callback(response)
          })
        }
        else  if (context == 0) {
          getResponse(6, function(response) {
            callback(response)
          })
        }
        else  if (context == 1 && action == null) {
          getResponse(7, function(response) {
            callback(response)
          })
        }
        else if (context == 2 && pizza == null && menu == null) {
          getResponse(7, function(response) {
            callback(response)
          })
        }
      }
    })
  })
}

module.exports = {

  start: function(text, sender_id, callback) {
    let pizza;
    let total;
    let name;
    getContext(sender_id, function(result) {
      if (result.length == 0) {
        createContext(sender_id, 1)
        notOrdered(text, sender_id, 0, function(response1, response2) {
          callback(response1, response2)
        })
      }
      else {
        context = parseInt(result[0].context)
        switch (context) {
          case 1:
            notOrdered(text, sender_id, context, function(response1, response2) {
              callback(response1, response2)
            })
            break;
          case 2:
            notOrdered(text, sender_id, context, function(response1, response2) {
              callback(response1, response2)
            })
            break;
          case 3:
            pizza = result[0].pizza
            total = parseInt(result[0].total)
            setContext(sender_id, 4, pizza, total, text)
            getResponse(3, function(response) {
              callback(response)
            })
            break;
          case 4:
            pizza = result[0].pizza
            name = result[0].name
            total = parseInt(result[0].total)
            let address = text
            createOrder(pizza, name, address, total)
            setContext(sender_id, 1)
            getResponse(5, function(response) {
              response = response.replace("%pizza", pizza);
              response = response.replace("%name", name);
              response = response.replace("%address", address);
              response = response.replace("%total", total);
              callback(response)
            })
            break;
        }
      }
    })
  }
}
