'use strict';

// Imports dependencies and set up http server
const
  express = require('express'),
  request = require('request'),
  bodyParser = require('body-parser'),
  pizzaBot = require('./pizzabot'),
  app = express().use(bodyParser.json()); // creates express http server

// Sets server port and logs message on success
app.listen(process.env.PORT || 1337, () => console.log('webhook is listening'));
// Creates the endpoint for our webhook
app.post('/webhook', (req, res) => {

  let body = req.body;

  // Checks this is an event from a page subscription
  if (body.object === 'page') {

    // Iterates over each entry - there may be multiple if batched
    body.entry.forEach(function(entry) {
      let url = 'https://graph.facebook.com/v2.6/me/messages?access_token=EAACPH7K1ey8BAPeM8ZC9Eo4gx2kH6PWZCV58AlscZBk2FdDZBPAd9tLxEe4vwwXESwGWWyzeNT7hcKIQMm5hlSpFrjwKOqhKtceZBJdUBf3OoZAj7UpSzZB3SxkZCECGtADTxq6fSTJkhkpNFyu8PZCHBpw2QGOlTnoWS33F5lYlLBAZDZD'
      let headers = {
        'Content-Type': 'application/json'
      }
      if (typeof entry.messaging[0].message.text != 'undefined')
        console.log(entry.messaging[0].message.text);
        pizzaBot.start(entry.messaging[0].message.text, entry.messaging[0].sender.id, function(response1, response2) {
          if (response1) {
            let response_msg = {
              "recipient":
                  {"id": entry.messaging[0].sender.id},
              "message":
                  {"text": response1}
                }
            let options = {
              method: 'POST',
               url: url,
               headers: headers,
               body: JSON.stringify(response_msg)
             }
             request(options, function(err, response, body){})
            console.log(response1);
            if (response2) {
              let response_msg = {
                "recipient":
                    {"id": entry.messaging[0].sender.id},
                "message":
                    {"text": response2}
                  }
              let options = {
                method: 'POST',
                 url: url,
                 headers: headers,
                 body: JSON.stringify(response_msg)
               }
               request(options, function(err, response, body){})
              console.log(response2);
            }
          }
        })
     })
    // Returns a '200 OK' response to all requests
    res.status(200).send('EVENT_RECEIVED');
  } else {
    // Returns a '404 Not Found' if event is not from a page subscription
    res.sendStatus(404);
  }

});

// Adds support for GET requests to our webhook
app.get('/webhook', (req, res) => {

  // Your verify token. Should be a random string.
  let VERIFY_TOKEN = "38290"

  // Parse the query params
  let mode = req.query['hub.mode'];
  let token = req.query['hub.verify_token'];
  let challenge = req.query['hub.challenge'];

  // Checks if a token and mode is in the query string of the request
  if (mode && token) {

    // Checks the mode and token sent is correct
    if (mode === 'subscribe' && token === VERIFY_TOKEN) {

      // Responds with the challenge token from the request
      console.log('WEBHOOK_VERIFIED');
      res.status(200).send(challenge);

    } else {
      // Responds with '403 Forbidden' if verify tokens do not match
      res.sendStatus(403);
    }
  }
});
