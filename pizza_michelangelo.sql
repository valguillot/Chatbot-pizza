-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 29, 2017 at 06:49 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizza_michelangelo`
--

-- --------------------------------------------------------

--
-- Table structure for table `context`
--

DROP TABLE IF EXISTS `context`;
CREATE TABLE IF NOT EXISTS `context` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` varchar(50) NOT NULL,
  `context` int(11) NOT NULL,
  `pizza` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `total` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `list_action`
--

DROP TABLE IF EXISTS `list_action`;
CREATE TABLE IF NOT EXISTS `list_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `list_action`
--

INSERT INTO `list_action` (`id`, `action`) VALUES
(1, 'order'),
(2, 'menu'),
(7, 'no_action'),
(6, 'greeting'),
(5, 'order_created'),
(4, 'request_name'),
(3, 'request_address');

-- --------------------------------------------------------

--
-- Table structure for table `order_list`
--

DROP TABLE IF EXISTS `order_list`;
CREATE TABLE IF NOT EXISTS `order_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pizza` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `payment-method` varchar(255) NOT NULL DEFAULT 'Pay on delivery',
  `total` decimal(10,0) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pizza_menu`
--

DROP TABLE IF EXISTS `pizza_menu`;
CREATE TABLE IF NOT EXISTS `pizza_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pizza_menu`
--

INSERT INTO `pizza_menu` (`id`, `name`, `price`) VALUES
(1, 'Margherita', '9'),
(2, 'Quattro Formaggi', '10'),
(3, 'Mexican', '10'),
(4, 'Hawaiiana', '10'),
(5, 'Pepperoni', '10'),
(6, 'Seafood', '11'),
(7, 'Meatball', '11'),
(8, 'Bacon', '11'),
(9, 'Sausage', '11'),
(10, 'Ultimate Chicken', '11');

-- --------------------------------------------------------

--
-- Table structure for table `response`
--

DROP TABLE IF EXISTS `response`;
CREATE TABLE IF NOT EXISTS `response` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_id` int(11) NOT NULL,
  `sentence` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `response`
--

INSERT INTO `response` (`id`, `action_id`, `sentence`) VALUES
(1, 1, 'You want order a pizza? Sure! Please choose in the list which one do you want!'),
(2, 1, 'Our pizza are the best! Please choose the pizza you want in the menu'),
(3, 2, 'The menu is here!'),
(4, 2, 'Here is our pizza!'),
(5, 4, 'Ok! Could you give us the name of the person who will eat that delicious pizza?'),
(6, 4, 'Got it! Could you give me a name for the order please?'),
(7, 3, 'The last step is your full address for the delivery, could you write your full address please?'),
(8, 5, 'We have everything! A %pizza pizza will be delivery at %address for %name at the door you will pay %total$ ! Thank you so much for your order! We hope you will enjoy your pizza and ask us for a new one!'),
(9, 6, 'Hello! Welcome to Pizza Michelangelo! What can I do for you?'),
(10, 7, 'I am sorry I did not understand what do you mean, do you want order or see the menu?'),
(11, 7, 'Sorry we did not understand your request, do you wanna place a new order or see the menu?');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
